<?php
    use BatoiPOP\Category;
    use BatoiPOP\Product;

    /*require_once($route_config.'products.php');*/
    require_once ($route_config.'menu.php');
    /*require_once ($route_config.'categories.php');*/

    $categories = [];
    if (strstr($_SERVER['SCRIPT_NAME'],'productes.php')){
        $productos = $query->selectAll('productes');
    }else {
        if (isset($_GET['pagina'])) {

            switch ($_GET['pagina']) {
                case 'all':
                    $productos = $query->selectAll('productes');
                    break;
                case 'start':
                    $productos = $query->selectWhere('productes', 'stars', 5);
                    break;
                case 'news':
                    $productos = $query->selectOrderDescLimit('productes',8, 'createdAt');
                    break;
                default:
                    $productos = $query->selectAllLimit('productes', 8);
                    break;
            }
        } else if (isset($_GET['category'])){
            $productos = $query->selectWhere('productes', 'category', $_GET['category']);
        }else {
            $productos = $query->selectAllLimit('productes', 8);
        }
    }
    $categories = $query->selectAll('categories');
    $categories = Category::import($categories);
    $products = Product::import($productos);
    
