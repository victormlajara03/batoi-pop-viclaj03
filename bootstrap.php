<?php
use BatoiPOP\QueryBuilder;
use BatoiPOP\Connection;

return new QueryBuilder(Connection::make());
