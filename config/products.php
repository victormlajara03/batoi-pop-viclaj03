<?php
    $products = [
        [
            'name'  => 'Huawei P10',
            'original_price' => 60,
            'discount_price' => 50,
            'stars' => 4,
            'sale' => 0
        ],
        [
            'name'  => 'Huawei Mate',
            'original_price' => 20,
            'discount_price' => 10,
            'stars' => 2,
            'sale' => 0
        ],
        [
            'name'  => 'Huawei P20',
            'original_price' => 90,
            'stars' => 4,
            'sale' => 0
        ],
        [
            'name'  => 'Huawei P30',
            'original_price' => 150,
            'stars' => 5,
            'sale' => 0
        ],
        [
            'name'  => 'MacBook Air M1',
            'original_price' => 900,
            'discount_price' => 800,
            'stars' => 5,
            'sale' => 0
        ],
        [
            'name'  => 'MacBook Air 2017',
            'original_price' => 500,
            'stars' => 4,
            'sale' => 1
        ],
        [
            'name'  => 'MacBook Air M1',
            'original_price' => 1000,
            'stars' => 5,
            'sale' => 1
        ],
        [
            'name'  => 'MacBook Air 2015',
            'original_price' => 300,
            'stars' => 3,
            'sale' => 0
        ],
    ];
