<?php
// load filp/whoops

    require(dirname(__FILE__) . "/vendor/autoload.php");
    session_start();
    use Whoops\Run;
    use Whoops\Handler\PrettyPageHandler;
    $query = require('bootstrap.php');
    $mail = require ('mail.php');
    $whoops = new Run;
    $whoops->pushHandler(new PrettyPageHandler);
    $whoops->register();

// routes
    $route_views = $_SERVER['DOCUMENT_ROOT'].'/../views/';
    $route_src = $_SERVER['DOCUMENT_ROOT'].'/../src/';
    $route_config = $_SERVER['DOCUMENT_ROOT'].'/../config/';
