<?php
require_once ('../kernel.php');
require_once ('../Services/loadService.php');

if (isPost() && cfsr()){
    try {
        $id = intval($_POST['id']);
        $query->deleteById('productes','id',$id);
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

header("Location: productes.php");

