<?php

require_once('../kernel.php');
require_once('../Services/loadService.php');


use BatoiPOP\exceptions\CheckFieldException;
use BatoiPOP\exceptions\PasswordIsNotSame;

/*Secion inicio*/
$errors = [];
$email = $_GET['email'];
$token_recup = $_GET['token'];
if (isPost() && cfsr()) {
    try {
        $password = isRequired('password');
        $password2 = isRequired('password2');
        if ($password != $password2) {
            throw new PasswordIsNotSame($password2);
        }
    } catch (CheckFieldException $e) {
        $errors[$e->getField()] = $e->getMessage();
    }

    if (!count($errors)) {
        try {
            $user = $query->selectWhereUnique('users', 'email', $email);
            if ($token_recup == $user->token_recup && $user->token_recup != '') {
                $token_recup = '';
                $password = password_hash($password,PASSWORD_DEFAULT);
                $query->update('users', compact('token_recup', 'password'), 'id', $user->id);
                header('Location: /login.php');
                die();
            } else{
                throw new Exception('Email o token incorrectos');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

loadView('forgot', compact('errors'));
