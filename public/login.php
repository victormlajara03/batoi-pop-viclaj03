<?php
    require_once ('../kernel.php');
    use BatoiPOP\exceptions\CheckFieldException;
    $errors = [];

    if (isPost() && cfsr()){
        try {
            $user = isRequired('user',$errors);
        }catch (CheckFieldException $e){
            $errors[] = $e->getMessage();
        }

        try {
            $password = isRequired('password',$errors);
        }catch (CheckFieldException $e){
            $errors[] = $e->getMessage();
        }

       if (!count($errors)){
           try {
               $user = $query->login('users',$user,$password);
           }catch (PDOException $e){
               $errors[] = $e->getMessage();
           }
           if ($user == null){
               echo "Contraseña equivocada";
           }else {
               $_SESSION['user'] = serialize($user);
               header('Location: /');
           }
        }
    }
    loadView('login',compact('errors'));
