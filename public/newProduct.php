<?php
    require_once ('../kernel.php');
    require_once ('../Services/loadService.php');
    use BatoiPOP\exceptions\CheckFieldException;
    use BatoiPOP\Product;

    $errors = [];
    if (isPost() && cfsr()){
        try {
            $name = isRequired('name');
            $dprice = isBetween('dprice',0);
            $price = isBetween('price',$dprice);
            $stars = isBetween('stars',1,5);
            $category = $_POST['category'];
            $photo = saveFile('photo','image/png','images');
        } catch (CheckFieldException $e){
            $errors[$e->getField()] = $e->getMessage();
        }
       if (!count($errors)){
           $validProduct = new Product(5,$name,compact('dprice','price','stars','category','photo'));
           $prueba = ['name' =>$validProduct->getName(),'discount_price'=>$validProduct->dprice,
               'original_price'=>$validProduct->price,'stars' =>$validProduct->stars,'img'=>$validProduct->photo,'category'=>$validProduct->category];
           try {
               $query->insert('productes',$prueba);
               loadView('validateProduct',compact('menu','validProduct','categories'));
               die();
           }catch (PDOException $e){
               echo $e->getMessage();
           }
           }
}

    loadView('newProduct',compact('menu','categories','errors'));
