<?php
require_once ('../kernel.php');
require_once('../Services/loadService.php');


if (isset($_SESSION['user']  )){
    $user = unserialize($_SESSION['user']);
    if ($user->rol == 0){
        header('Location: /');
        die();
    }
} else{
    header('Location: /');
    die();
}

loadView('productes',compact('menu','products','categories'));


