<?php
require_once ('../kernel.php');
require_once('../Services/loadService.php');

use BatoiPOP\exceptions\CheckFieldException;

$errors = [];
if (isPost() && cfsr()) {
    try {
        $email = isRequired('email');
    } catch (CheckFieldException $e) {
        $errors[$e->getField()] = $e->getMessage();
    }
    try {
        if (!count($errors)) {
            $user = $query->selectWhereUnique('users', 'email', $email);
            if (!$user) {
                throw new Exception('No existe el usuario');
            }
            $token_recup = generar_token_seguro(60);
            $query->update('users',compact('token_recup'),'id',$user->id);
            $mail->setFrom('gemailfacherobatoipop@gmail.com','Batoi');
            $mail->addAddress($user->email,$user->name);
            $mail->isHTML(true);
            $mail->Subject = 'Forgot Password';
            $mail->Body = "Utiliza el seguent
            <a href='http://projecte.dwes.my/forgot.php?email=" . $user->email .
                "&token=" . $token_recup . "'>enlace</a>";
            $mail->send();
            echo 'Mensaje enviado';
        }
    }catch (Exception $e){
        echo $e->getMessage();
    }
}





loadView('recuperacion',compact('menu','products','categories'));
