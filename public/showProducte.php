<?php
require_once ('../kernel.php');
require_once ('../Services/loadService.php');
use BatoiPOP\exceptions\CheckFieldException;
use BatoiPOP\Product;
if (isPost() && cfsr()){
    try {
        $id = intval($_POST['id']);
        $validProduct = $query->findById('productes',$id);
    }catch (CheckFieldException $e){
        echo $e->getMessage();
    }

    $name = $validProduct['name'];
    $dprice = $validProduct['discount_price'];
    $price = $validProduct['original_price'];
    $stars = $validProduct['stars'];
    $category = $validProduct['category'];
    $photo = $validProduct['img'];
    $validProduct = new Product($id,$name,compact('dprice','price','stars','category','photo'));
    loadView('validateProduct',compact('menu','validProduct','categories'));
}