<?php
require_once ('../kernel.php');
require_once ('../Services/loadService.php');
use BatoiPOP\exceptions\CheckFieldException;
use BatoiPOP\Product;


try {
    $id = intval($_POST['id']);
    $productoCambio = $query->findById('productes',$id);
}catch (PDOException $e){
    echo $e;
    header("Location: productes.php");
    die();
}
$errors = [];
if (isPost() && cfsr()){
    try {
        $name = isRequired('name');
        $dprice = isBetween('dprice',0);
        $price = isBetween('price',$dprice);
        $stars = isBetween('stars',1,5);
        $category = $_POST['category'];
        $photo = saveFile('photo','image/png','images');
    } catch (CheckFieldException $e){
        $errors[$e->getField()] = $e->getMessage();
    }

    if (!count($errors)){
        $validProduct = new Product($id,$name,compact('dprice','price','stars','category','photo'));
        $prueba = ['name' =>$validProduct->getName(),'discount_price'=>$validProduct->dprice,
            'original_price'=>$validProduct->price,'stars' =>$validProduct->stars,'img'=>$validProduct->photo,'category'=>$validProduct->category];
        try {
            $query->update('productes',$prueba,'id',$id);
            loadView('validateProduct',compact('menu','validProduct','categories'));
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }
}
loadView('updateProducte',compact('menu','categories','errors','productoCambio'));
