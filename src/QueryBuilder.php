<?php


namespace BatoiPOP;

class QueryBuilder
{
    protected $conn;

    /**
     * QueryBuilder constructor.
     * @param $conn
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function selectAll($table){
        $stpdo = $this->conn->prepare("SELECT * FROM {$table}");

        $stpdo->execute();
        return $stpdo->fetchAll(\PDO::FETCH_OBJ);
    }

    public function selectAllLimit($table,$limit){
        $stpdo = $this->conn->prepare("SELECT * FROM {$table} LIMIT :limit");
        $stpdo->bindValue(":limit",$limit,\PDO::PARAM_INT);
        $stpdo->execute();
        return $stpdo->fetchAll(\PDO::FETCH_OBJ);
    }

    public function findById($table,$id){
        $stpdo = $this->conn->prepare("SELECT * FROM {$table} WHERE `id` = :id ");
        $stpdo->bindParam(":id",$id);
        $stpdo->execute();

        return $stpdo->fetch(\PDO::FETCH_ASSOC);
    }

    public function deleteById($table,$primaryKey,$id){
        $stpdo = $this->conn->prepare("DELETE FROM {$table} WHERE `$primaryKey` = :id ");
        $stpdo->bindParam(":id",$id);
        return $stpdo->execute();

    }

    public function selectWhere($table,$key,$value){
        $stpdo = $this->conn->prepare("SELECT * FROM {$table} WHERE `$key` = :value ");
        $stpdo->bindParam(":value",$value);
        $stpdo->execute();

        return $stpdo->fetchAll(\PDO::FETCH_OBJ);
    }

    public function selectOrderDescLimit($table,$limit,$orden){
        $stpdo = $this->conn->prepare("SELECT * FROM {$table}  ORDER BY ${orden} DESC LIMIT :limit");
        $stpdo->bindValue(":limit",$limit,\PDO::PARAM_INT);
       /* $stpdo->bindValue(":orden",$orden,\PDO::PARAM_STR);*/
        $stpdo->execute();
        return $stpdo->fetchAll(\PDO::FETCH_OBJ);
    }

    public function insert($table,$parametres){
        $stpdo = $this->conn->prepare(insert($table,$parametres));
        foreach ($parametres as $key => $value){
            $stpdo->bindValue(":$key",$value);
        }
        $stpdo->execute();
    }

    public function update($table,$parametres,$primaryKey,$id){
        $stpdo = $this->conn->prepare(update($table,$parametres,$primaryKey));
        foreach ($parametres as $key => $value){
            $stpdo->bindValue(":$key",$value);
        }
        $stpdo->bindParam(":id",$id);
        $stpdo->execute();
    }

    public function login($table,$email,$password){
        $stpdo = $this->conn->prepare("SELECT * FROM $table WHERE email = :email");
        $stpdo->bindValue(":email",$email);
        $stpdo->execute();
        $user = $stpdo->fetch(\PDO::FETCH_OBJ);
        if (!$user){
            echo "No existe usuario";
        } else {
            if (password_verify($password, $user->password)) return $user;
            return null;
        }
    }

    public function selectWhereUnique($table,$key,$value){
        $stpdo = $this->conn->prepare("SELECT * FROM {$table} WHERE `$key` = :value LIMIT 1 ");
        $stpdo->bindParam(":value",$value);
        $stpdo->execute();

        return $stpdo->fetch(\PDO::FETCH_OBJ);
    }

/*



* deleteById(nomTaula,id)
*/

}