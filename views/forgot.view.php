<?php
    require_once('partials/head.view.php');
    require_once('partials/header.view.php');
?>
<!-- Section-->
<section class="py-5">
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            <form method="POST" action="<?=$_SERVER['REQUEST_URI']?>" >
                <div class="form-group">
                    <input name="password" type="password" class="form-control" id="password"  placeholder="new Password">
                </div>
                <div class="form-group">
                    <input name="password2" type="password" class="form-control <?= isValidClass('password2',$errors) ?>" id="password2"  placeholder="Repeate password">
                    <?= showError('password2',$errors) ?>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</section>
<?php
    require_once('partials/footer.view.php');
?>