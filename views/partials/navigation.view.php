<body>
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container px-4 px-lg-5">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                <?php  foreach ($menu as $group): ?>
                    <?php $nameLink = key($group);  ?>
                    <?php $link = $group[$nameLink]; ?>
                    <?php if (!is_array($link)): ?>
                        <li class="nav-item"><a class="nav-link active" aria-current="page"
                                                href="<?= $link ?>">
                                <?= $nameLink ?></a></li>
                    <?php else: ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><?= $nameLink ?></a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <?php foreach ($link as $subName => $subLink): ?>
                                    <li><a class="dropdown-item" href="<?= $subLink ?>"><?= $subName ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php endif ?>
                <?php endforeach; ?>
            </ul>
            <?php if (isset($_SESSION['user'])){
                $user = unserialize($_SESSION['user']);
                ?>
                <h2><?=$user->name?></h2>
                <form class="d-flex">
                    <button class="btn btn-outline-dark" type="submit">
                        <i class="bi bi-person"><a class="dropdown-item" href="logout.php">logout</a></i>
                    </button>
                </form>
            <?php }else {?>
            <form class="d-flex">
                <button class="btn btn-outline-dark" type="submit">
                    <i class="bi bi-person"><a class="dropdown-item" href="login.php">Login</a></i>
                </button>
            </form>
            <?php } ?>
        </div>
    </div>
</nav>