<?php
require_once('partials/head.view.php');
require_once('partials/navigation.view.php');
require_once('partials/header.view.php');
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12" id="almacen">
        <table class="table table-striped table-hover">
            <thead class="thead-dark bg-light">
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Precio Original</th>
                <th>Precio Descuento </th>
                <th>Estrellas</th>
                <th>Sale</th>
                <th> Acciones </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $product){?>
            <tr>
                <td> <?=$product->getId()?></td>
                <td> <?=$product->getName()?></td>
                <td> <?=$product->original_price?></td>
                <td> <?=$product->discount_price?></td>
                <td> <?=$product->stars?></td>
                <td> <?=$product->sale?"no":"si"?></td>
                <td>
                    <form method="POST" action="deleteProducte.php">
                        <input type="hidden" value="<?=$product->getId()?>" name="id" id="id">
                        <button class="btn btn-delete">
                            <span class="material-icons">delete</span>
                        </button>
                    </form>
                </td>
                <td>
                    <form method="post" action="updateProducte.php">
                        <input type="hidden" value="<?=$product->getId()?>" name="id" id="id">
                    <button class="btn btn-edit">
                        <span class="material-icons">edit</span>
                    </button>
                    </form>
                </td>
                <td>
                    <form method="post" action="showProducte.php">
                        <input type="hidden" value="<?=$product->getId()?>" name="id" id="id">
                        <button class="btn btn-edit">
                        <span class="material-icons">visibility</span>
                    </button>
                    </form>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<br>
<form class="d-flex">
    <button class="btn btn-outline-dark" type="submit">
        <i ><a class="dropdown-item" href="newProduct.php">Add Product</a></i>
    </button>
</form>
<br>

<?php
require_once('partials/footer.view.php');
?>
