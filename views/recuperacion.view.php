<?php
    require_once('partials/head.view.php');
    require_once('partials/header.view.php');
?>
<!-- Section-->
<section class="py-5">
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            <form method="POST" action="recuperacion.php" >
                <div class="form-group">
                    <label for="email">email:</label>
                    <input name="email" type="text" class="form-control" id="email"  placeholder="Enter user">
                </div>
                <a href="register.php" class="btn btn-dark">Register</a>
                <button type="submit" class="btn btn-primary">recuperar</button>
            </form>
        </div>
    </div>
</section>
<?php
    require_once('partials/footer.view.php');
?>